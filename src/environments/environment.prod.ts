export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyCfYPPmtfUIhkg1sqKctM14iDX-T1iV_s0",
    authDomain: "ptweb-c7ea2.firebaseapp.com",
    databaseURL: "https://ptweb-c7ea2-default-rtdb.firebaseio.com",
    projectId: "ptweb-c7ea2",
    storageBucket: "ptweb-c7ea2.appspot.com",
    messagingSenderId: "962108825794",
    appId: "1:962108825794:web:95211b2dfae1a1b967db5b",
    measurementId: "G-WS3TYRSMN7"
  }
};
