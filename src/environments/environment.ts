// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCfYPPmtfUIhkg1sqKctM14iDX-T1iV_s0",
    authDomain: "ptweb-c7ea2.firebaseapp.com",
    databaseURL: "https://ptweb-c7ea2-default-rtdb.firebaseio.com",
    projectId: "ptweb-c7ea2",
    storageBucket: "ptweb-c7ea2.appspot.com",
    messagingSenderId: "962108825794",
    appId: "1:962108825794:web:95211b2dfae1a1b967db5b",
    measurementId: "G-WS3TYRSMN7"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
