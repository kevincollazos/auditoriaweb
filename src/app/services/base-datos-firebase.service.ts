import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database'

//models
import { UsuarioModel } from "../models/usuario.model";
import { ProjectModel } from "../models/project.model";
import { EmpresaModel } from "../models/empresa.model";
import { HistoryModel } from "../models/history.model";
import { TicketModel } from "../models/ticket.model";

@Injectable({
  providedIn: 'root'
})
export class BaseDatosFirebaseService {

  usersList: AngularFireList<any>;
  projectsList: AngularFireList<any>;
  historyList: AngularFireList<any>;
  ticketsList: AngularFireList<any>;
  enterpiseList: AngularFireList<any>;
  url: string = 'https://ptweb-c7ea2-default-rtdb.firebaseio.com/'

  constructor( private db: AngularFireDatabase, private http:HttpClient ) { }

  /**
   * Metodo para obtener todos los usuario
   * @returns arreglo con los usuarios de la bd
   */
  getUsers(){
    return this.usersList = this.db.list('tb_usuarios');
  }

  /**
   * Metodo para registrar un usuario
   * @param user modelo el cual contiene la info del usuario
   * @returns Arreglo con el registro de usuario creado
   */
  registerUser( user: UsuarioModel )
  {
    return this.usersList.push({
      nombre: user.nombre,
      apellido:user.apellido,
      email: user.email,
      password: user.password,
      empresa: user.empresa,
      fechaRegistro: user.fechaRegistro = new Date().getTime().toString(),
    }).then((res: any)=>{
      user.ref = res.key;
      return user;
    });
  }

  /**
   * Metodo para obtener todos los proyectos
   * @returns lista de proyectos
   */
  getProjects(){
    return this.projectsList = this.db.list('tb_proyectos');
  }

  /**
   * Metodo para crear un proyecto
   * @param project informacion del proyecto
   * @returns lista de proyectos
   */
  createProject( project: ProjectModel ){
    return this.projectsList.push({
      nombre: project.nombre,
      descripcion: project.descripcion,
      idUser: localStorage.getItem('email')
    });
  }

  /**
   * Metodo para obtener las historias
   * @returns lista de historias
   */
  getHistory(){
    return this.historyList = this.db.list('tb_historias');
  }

  /**
   * Metodo para registrar una historia
   * @param history data de la historia
   * @returns lista de history
   */
  createHistory(history: HistoryModel){
    return this.historyList.push({
      nombre: history.nombre,
      descripcion: history.descripcion,
      idProject: localStorage.getItem("idProject"),
      idUser: localStorage.getItem("email")
    });
  }
  
  /**
   * Metodo para obtener los tickets
   * @returns lista de tickets
   */
  getTickets(){
    return this.ticketsList = this.db.list('tb_tickets');
  }

  /**
   * Metodo para obtener un solo ticket
   * @param id id del ticket
   * @returns lista de ticket seleccionado
   */
  getTicket(id:string){
    return this.http.get(`${this.url}/tb_tickets/${id}.json`)
  }

  /**
   * Metodo para crear un ticket
   * @param ticket informacion del ticket
   * @returns lista de ticket
   */
  createTicket(ticket: TicketModel){
    return this.ticketsList.push({
      nombre: ticket.nombre,
      comentario: ticket.comentario,
      estado: ticket.estado = "Activo",
      idProject: localStorage.getItem("idProject"),
      idHistory: localStorage.getItem("idHistory"),
      idUser: localStorage.getItem("email")
    });
  }

  updateTicket( ticket: TicketModel){
    return this.ticketsList.update( ticket.id, {
        nombre: ticket.nombre,
        comentario:ticket.comentario,
        estado: ticket.estado
    }).then( () => {      
      return ticket;
    });
  }

  /**
   * Metodo para obtener las empresas
   * @returns lista de empresas
   */
  getEnterprises(){
    return this.enterpiseList = this.db.list('empresas');
  }

  /**
   * Metodo para crear una empresa
   * @param enterprise informacion de la empresa
   * @returns lista de empresas
   */
   createEnterprises( enterprise: EmpresaModel ){
    return this.enterpiseList.push({
      nit: enterprise.nit,
      nombre: enterprise.nombre,
      email: enterprise.email,
      telefono: enterprise.telefono,
      direccion: enterprise.direccion
    });
  }

}
