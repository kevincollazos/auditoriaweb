import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UsuarioModel } from '../models/usuario.model';

import { map } from 'rxjs/operators'


@Injectable({
  providedIn: 'root'
})
export class AuthService {


  private url = 'https://identitytoolkit.googleapis.com/v1/accounts:';
  private apikey ='AIzaSyCfYPPmtfUIhkg1sqKctM14iDX-T1iV_s0';

  userToken: string;
  emailToken: string;

  constructor( private http: HttpClient ) {
    this.leerToken();
   }

   logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('expira');
    localStorage.removeItem('idProject');
    localStorage.removeItem('idHistory');
    localStorage.removeItem('email');
   }

  /**
    * metodo para validar el login del usuario
    * @param usuario usuario que hace login
    * @returns 
    */
  login(usuario: UsuarioModel) {

    const authData = {
      ...usuario,
      returnSecureToken: true
    };
    
    return this.http.post(
      `${ this.url }signInWithPassword?key=${this.apikey}`,
      authData
     ).pipe (
      map( resp=> {        
        this.guardarToken( resp['idToken'], resp['email']);
        return resp;
      })
    );
 
   }

   /**
    * Metodo para realizar el login cuando se registra un usuario
    * @param usuario datos del usuario
    * @returns 
    */
   nuevoUsuario (usuario: UsuarioModel) {

    const authData = {
      ...usuario,
      returnSecureToken: true
    };

    return this.http.post(
     `${ this.url }signUp?key=${this.apikey}`,
     authData
    ).pipe (
      map( resp=> {
        this.guardarToken( resp['idToken'], resp['email']);
        return resp;
      })
    );
   }

   /**
    * metodo para guardar token
    * @param idToken token
    */
   private guardarToken (idToken: string, email: string) {
    this.userToken = idToken;
    this.emailToken = email;
    
    localStorage.setItem('token', idToken);
    localStorage.setItem( "email", email );

    let hoy = new Date();
    hoy.setSeconds( 3600 );

    localStorage.setItem('expira', hoy.getTime().toString() );
   }

   /**
    * Metodo para leer el token
    * @returns el token del usuario
    */
   leerToken(){

    if (localStorage.getItem('token') ){
      this.userToken = localStorage.getItem('token');
    }else{
      this.userToken = '';
    }

    return this.userToken;

   }

   /**
    * Metodo para saber si el usuario esta autenticado
    * @returns verdareo o falso
    */
   estaAutenticado(): boolean {

    if ( this.userToken.length < 2 ) {
      return false;
    }

    const expira = Number(localStorage.getItem('expira'));
    const expiraDate = new Date();
    expiraDate.setTime(expira);

    if ( expiraDate > new Date() ) {
      return true;
    } else {
      this.logout();
      return false;
    }


   }
}
