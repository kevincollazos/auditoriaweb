import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2'
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

//database
import { BaseDatosFirebaseService } from '../../services/base-datos-firebase.service'

//models
import { HistoryModel } from 'src/app/models/history.model';

@Component({
  selector: 'app-create-history',
  templateUrl: './create-history.component.html',
  styleUrls: ['./create-history.component.css']
})
export class CreateHistoryComponent implements OnInit {

  history: HistoryModel = new HistoryModel();
  idProject = this.route.snapshot.paramMap.get('id');

  constructor( private db: BaseDatosFirebaseService, private router: Router, private route: ActivatedRoute ) { }

  ngOnInit(): void {
    this.db.getHistory();
  }

  onSubmit( form:NgForm ){
    if ( form.invalid ){ return }
     //alerta
     Swal.fire({
      allowOutsideClick: false,
      icon: 'info',
      text: 'Por favor espere'
    });
    Swal.showLoading();
    this.db.createHistory(this.history).then( resp2 => {  

      Swal.fire({
        icon: 'success',
        title: 'Se creo la historia',
        text: 'Se ha redirigido a sus historias'
      });    
      this.router.navigateByUrl(`project/${this.idProject}`);
    }) , (err) => {
      Swal.fire({
        icon: 'error',
        title: 'Error al guardar',
        text: err.error.error.message
      });
      
    }
  }

  goBack(id:string){
    this.router.navigateByUrl(`project/${id}`);
  }

}
