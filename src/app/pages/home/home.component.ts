import { Component, OnInit } from '@angular/core';

//DataBase
import { BaseDatosFirebaseService } from '../../services/base-datos-firebase.service'

//Models
import { ProjectModel } from "../../models/project.model";


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  projectList: ProjectModel[];

  constructor(private db: BaseDatosFirebaseService) {  }

  ngOnInit(): void { 
    this.db.getProjects().snapshotChanges().subscribe(
      item => {
        this.projectList = [];
        item.forEach(element =>{
          let project = element.payload.toJSON();
          if (project["idUser"] == localStorage.getItem('email') ) {
            project["id"] = element.key;
          this.projectList.push(project as ProjectModel);
          }          
        });
      }
    );
   }

}
