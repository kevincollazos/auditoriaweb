import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2'
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

//database
import { BaseDatosFirebaseService } from '../../services/base-datos-firebase.service'

//models
import { TicketModel } from 'src/app/models/ticket.model';

@Component({
  selector: 'app-edit-ticket',
  templateUrl: './edit-ticket.component.html',
  styleUrls: ['./edit-ticket.component.css']
})
export class EditTicketComponent implements OnInit {

  ticket: TicketModel = new TicketModel();
  idProject = this.route.snapshot.paramMap.get('id');
  idHistory = this.route.snapshot.paramMap.get('idH');
  
  estadosList = ['Activo','En Proceso','Finalizado'];

  constructor( private db: BaseDatosFirebaseService, private router: Router, private route: ActivatedRoute ) { }

  ngOnInit(): void {
    this.db.getTickets();
    const idTicket = this.route.snapshot.paramMap.get('idT');
    this.db.getTicket(idTicket).subscribe((res:TicketModel)=>{
      this.ticket = res;
      this.ticket.id = idTicket;
    });
  }

  onSubmit( form:NgForm ){
    if ( form.invalid ){ return }
     //alerta
     Swal.fire({
      allowOutsideClick: false,
      icon: 'info',
      text: 'Por favor espere'
    });
    Swal.showLoading();
    this.db.updateTicket(this.ticket).then( resp2 => {  
      resp2.id
      Swal.fire({
        icon: 'success',
        title: 'Se actualizo la historia',
        text: 'Se ha redirigido a su Tickets'
      });    
      this.router.navigateByUrl(`project/${this.idProject}/${this.idHistory}`);
    }) , (err) => {
      Swal.fire({
        icon: 'error',
        title: 'Error al actualizar',
        text: err.error.error.message
      }); 
    }
  }

  
  goBack(idP: string, idH: string){
    this.router.navigateByUrl(`project/${idP}/${idH}`);
  }

}
