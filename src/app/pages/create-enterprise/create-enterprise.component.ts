import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2'
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

//models
import { EmpresaModel } from "src/app/models/empresa.model";

//database
import { BaseDatosFirebaseService } from '../../services/base-datos-firebase.service'

@Component({
  selector: 'app-create-enterprise',
  templateUrl: './create-enterprise.component.html',
  styleUrls: ['./create-enterprise.component.css']
})
export class CreateEnterpriseComponent implements OnInit {

  company: EmpresaModel = new EmpresaModel();

  constructor( private db: BaseDatosFirebaseService, private router: Router ) { }

  ngOnInit(): void {
    this.db.getEnterprises();
  }

  onSubmit( form:NgForm ){
    if ( form.invalid ){ return }
     //alerta
     Swal.fire({
      allowOutsideClick: false,
      icon: 'info',
      text: 'Por favor espere'
    });
    Swal.showLoading();
    this.db.createEnterprises(this.company).then( resp2 => {  

      Swal.fire({
        icon: 'success',
        title: 'Se creo la empresa correctamente',
        text: 'Sera dirigido a la lista de empresas'
      });
    
      this.router.navigateByUrl('/companys')

    }) , (err) => {
      Swal.fire({
        icon: 'error',
        title: 'Error al guardar',
        text: err.error.error.message
      });
      
    }
  }

}
