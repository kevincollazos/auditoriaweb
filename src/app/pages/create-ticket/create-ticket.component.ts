import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2'
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

//database
import { BaseDatosFirebaseService } from '../../services/base-datos-firebase.service'

//models
import { TicketModel } from 'src/app/models/ticket.model';

@Component({
  selector: 'app-create-ticket',
  templateUrl: './create-ticket.component.html',
  styleUrls: ['./create-ticket.component.css']
})
export class CreateTicketComponent implements OnInit {

  ticket: TicketModel = new TicketModel();
  idProject = this.route.snapshot.paramMap.get('id');
  idHistory = this.route.snapshot.paramMap.get('idH');

  constructor( private db: BaseDatosFirebaseService, private router: Router, private route: ActivatedRoute ) { }

  ngOnInit(): void {
    this.db.getTickets();
  }

  onSubmit( form:NgForm ){
    if ( form.invalid ){ return }
     //alerta
     Swal.fire({
      allowOutsideClick: false,
      icon: 'info',
      text: 'Por favor espere'
    });
    Swal.showLoading();
    this.db.createTicket(this.ticket).then( resp2 => {  

      Swal.fire({
        icon: 'success',
        title: 'Se creo la historia',
        text: 'Se ha redirigido a sus historias'
      });    
      this.router.navigateByUrl(`project/${this.idProject}/${this.idHistory}`);
    }) , (err) => {
      Swal.fire({
        icon: 'error',
        title: 'Error al guardar',
        text: err.error.error.message
      });
      
    }
  }

  goBack(id:string, idH:string){
    this.router.navigateByUrl(`project/${id}/${idH}`);
  }

}
