import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnterpisesComponent } from './enterpises.component';

describe('EnterpisesComponent', () => {
  let component: EnterpisesComponent;
  let fixture: ComponentFixture<EnterpisesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnterpisesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnterpisesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
