import { Component, OnInit } from '@angular/core';

//DataBase
import { BaseDatosFirebaseService } from '../../services/base-datos-firebase.service'

//Model
import { EmpresaModel } from "../../models/empresa.model";

@Component({
  selector: 'app-enterpises',
  templateUrl: './enterpises.component.html',
  styleUrls: ['./enterpises.component.css']
})
export class EnterpisesComponent implements OnInit {

  enterpriseList: EmpresaModel[];

  constructor( private db: BaseDatosFirebaseService ) { }

  ngOnInit(): void {
    this.db.getEnterprises().snapshotChanges().subscribe(
      item => {
        this.enterpriseList = [];
        item.forEach(element =>{
          let enterprise = element.payload.toJSON();
          enterprise["id"] = element.key;
          this.enterpriseList.push(enterprise as EmpresaModel);
        });
      }
    );
  }

}
