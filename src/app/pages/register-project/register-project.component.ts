import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2'
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

//database
import { BaseDatosFirebaseService } from '../../services/base-datos-firebase.service'

//models
import { ProjectModel } from 'src/app/models/project.model';

@Component({
  selector: 'app-register-project',
  templateUrl: './register-project.component.html',
  styleUrls: ['./register-project.component.css']
})
export class RegisterProjectComponent implements OnInit {

  project: ProjectModel = new ProjectModel();

  constructor( private db: BaseDatosFirebaseService, private router: Router ) { }

  ngOnInit(): void {
    this.db.getProjects();
  }

  onSubmit( form:NgForm ){
    if ( form.invalid ){ return }
     //alerta
     Swal.fire({
      allowOutsideClick: false,
      icon: 'info',
      text: 'Por favor espere'
    });
    Swal.showLoading();
    this.db.createProject(this.project).then( resp2 => {  

      Swal.fire({
        icon: 'success',
        title: 'Se creo el proyecto correctamente',
        text: 'Se ha redirigido al home'
      });
    
      this.router.navigateByUrl('/home')

    }) , (err) => {
      Swal.fire({
        icon: 'error',
        title: 'Error al guardar',
        text: err.error.error.message
      });
      
    }
  }

}
