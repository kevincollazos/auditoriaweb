import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

//DataBase
import { BaseDatosFirebaseService } from '../../services/base-datos-firebase.service'

//Models
import { TicketModel } from "../../models/ticket.model";

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.css']
})
export class TicketComponent implements OnInit {

  idProject = this.route.snapshot.paramMap.get('id');
  idHistory = this.route.snapshot.paramMap.get('idH');

  ticketList: TicketModel[];

  constructor( private route: ActivatedRoute, private db: BaseDatosFirebaseService, private router: Router ) { }

  ngOnInit(): void {
    localStorage.setItem("idHistory", this.idHistory);
    this.db.getTickets().snapshotChanges().subscribe(
      item => {
        this.ticketList = [];
        item.forEach(element =>{
          let ticket = element.payload.toJSON();
          if ( ticket["idProject"] == localStorage.getItem('idProject') && ticket["idHistory"] == localStorage.getItem("idHistory") ) {
            ticket["id"] = element.key;  
            this.ticketList.push(ticket as TicketModel);
          }          
        });
      }
    );
  }

  /**
   * Metodo para navegar al create ticket
   * @param idP id del proyecto
   * @param idH id de la historia
   */
  navigateCreateTicket(idP:string, idH:string){
    this.router.navigateByUrl(`project/${idP}/${idH}/create-ticket`);
    
  }

  /**
   * Metodo para editar un ticket
   * @param idP id de proyecto
   * @param idH id de la historia
   * @param idT id del ticket
   */
  goToEditTickets(idP:string, idH:string, idT:string){
    this.router.navigateByUrl(`project/${idP}/${idH}/edit-ticket/${idT}`);
  }
}
