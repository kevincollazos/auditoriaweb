import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

//DataBase
import { BaseDatosFirebaseService } from '../../services/base-datos-firebase.service'

//Models
import { TicketModel } from "../../models/ticket.model";

@Component({
  selector: 'app-history-ticket',
  templateUrl: './history-ticket.component.html',
  styleUrls: ['./history-ticket.component.css']
})
export class HistoryTicketComponent implements OnInit {

  ticketList: TicketModel[];

  constructor( private route: ActivatedRoute, private db: BaseDatosFirebaseService, private router: Router ) { }

  ngOnInit(): void {
    this.db.getTickets().snapshotChanges().subscribe(
      item => {
        this.ticketList = [];
        item.forEach(element =>{
          let ticket = element.payload.toJSON();
          ticket["id"] = element.key;  
          this.ticketList.push(ticket as TicketModel);         
        });
      }
    );
  }

}
