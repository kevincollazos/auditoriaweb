import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2'
import { NgForm } from '@angular/forms';

//database
import { BaseDatosFirebaseService } from '../../services/base-datos-firebase.service'

//models
import { UsuarioModel } from 'src/app/models/usuario.model';


//services
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user: UsuarioModel = new UsuarioModel();
  empresas = ['Empresa 1', 'Empresa 2', 'Empresa 3'];

  constructor( private db: BaseDatosFirebaseService, private auth: AuthService ) { }

  ngOnInit(): void {
    this.db.getUsers();
  }

  onSubmit( form:NgForm ){
    if ( form.invalid ){ return }
     //alerta
     Swal.fire({
      allowOutsideClick: false,
      icon: 'info',
      text: 'Por favor espere'
    });
    Swal.showLoading();
      this.auth.nuevoUsuario(this.user)
       .subscribe( res => {
        this.db.registerUser(this.user)
        .then( resp =>  {
          this.user = resp;
        });
        Swal.close();

        Swal.fire({
          title: 'Usuario Creado con exito',
          text: "Se redirigira a la pagina principal",
          icon: 'success'
        })
      }, (err) => {
        console.log(err.error.error.message);
        Swal.fire({
          icon: 'error',
          text: err.error.error.message,
          title: 'Error al Registrar el usuario'
        });
      });
  }

}
