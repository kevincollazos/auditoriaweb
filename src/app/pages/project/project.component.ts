import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

//DataBase
import { BaseDatosFirebaseService } from '../../services/base-datos-firebase.service'

//Models
import { HistoryModel } from "../../models/history.model";


@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {

  idProject = this.route.snapshot.paramMap.get('id');
  historyList: HistoryModel[];

  constructor( private route: ActivatedRoute, private db: BaseDatosFirebaseService, private router: Router )  { }

  ngOnInit(): void {    
    localStorage.setItem("idProject", this.idProject);
    this.db.getHistory().snapshotChanges().subscribe(
      item => {
        this.historyList = [];
        item.forEach(element =>{
          let history = element.payload.toJSON();
          if (history["idProject"] == localStorage.getItem('idProject') ) {
            history["id"] = element.key;  
            this.historyList.push(history as HistoryModel);
          }          
        });
      }
    );
  }

  /**
   * Navegacion hacia create history
   */
  navigateCreateHistory(id:string){
    this.router.navigateByUrl(`project/${id}/create-history`);
  }

  /**
   * Navegacion hacia tickets
   */
  goToTickets(id:string, idH:string){
    this.router.navigateByUrl(`project/${id}/${idH}`);   
  }
}
