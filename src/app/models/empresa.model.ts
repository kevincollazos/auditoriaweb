export class EmpresaModel {
    id:string
    nombre:string;
    nit:string;
    telefono:string;
    direccion:string;
    email:string;
}