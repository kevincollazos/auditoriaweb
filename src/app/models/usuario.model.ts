export class UsuarioModel {
    id: string;
    email: string;
    password: string;
    nombre: string;
    apellido:string;
    ref:string;
    empresa: string;
    fechaRegistro:string;
}