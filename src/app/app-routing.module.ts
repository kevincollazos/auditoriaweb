import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//Pages
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component'
import { ProjectComponent } from "./pages/project/project.component";
import { RegisterProjectComponent } from "./pages/register-project/register-project.component";
import { CreateHistoryComponent } from "./pages/create-history/create-history.component";
import { TicketComponent } from "./pages/ticket/ticket.component";
import { CreateTicketComponent } from "./pages/create-ticket/create-ticket.component";
import { EditTicketComponent } from "./pages/edit-ticket/edit-ticket.component";
import { EnterpisesComponent } from "./pages/enterpises/enterpises.component";
import { CreateEnterpriseComponent } from "./pages/create-enterprise/create-enterprise.component";
import { HistoryTicketComponent } from "./pages/history-ticket/history-ticket.component";

//guardia
import { AuthGuard } from './guards/auth.guard';


const routes: Routes = [
  {path:'home', component: HomeComponent, canActivate: [ AuthGuard ] },
  {path:'login', component: LoginComponent},
  {path:'register', component: RegisterComponent},
  {path:'project/:id', component: ProjectComponent, canActivate:[ AuthGuard ]},
  {path:'register-project', component: RegisterProjectComponent, canActivate:[ AuthGuard ]},
  {path:'project/:id/create-history', component: CreateHistoryComponent, canActivate:[ AuthGuard ]},
  {path:'project/:id/:idH', component: TicketComponent, canActivate:[ AuthGuard ]},
  {path:'project/:id/:idH/create-ticket', component: CreateTicketComponent, canActivate:[ AuthGuard ]},
  {path:'project/:id/:idH/edit-ticket/:idT', component: EditTicketComponent, canActivate:[ AuthGuard ]},
  {path:'history-tickets', component: HistoryTicketComponent, canActivate:[ AuthGuard ]},
  {path:'companys', component: EnterpisesComponent, canActivate:[ AuthGuard ]},
  {path:'register-company', component: CreateEnterpriseComponent, canActivate:[ AuthGuard ]},
  {path: '**', pathMatch: 'full', redirectTo: 'home'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash:true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
