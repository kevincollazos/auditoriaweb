import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';


import { NavbarComponent } from './components/navbar/navbar.component';
import { FooterComponent } from './components/footer/footer.component';


//Firebase
import { AngularFireModule } from 'angularfire2'
import { AngularFireDatabaseModule } from 'angularfire2/database'
import { AngularFirestoreModule } from '@angular/fire/firestore';

//environment
import { environment } from '../environments/environment';

//pipe
import { SafeUrlPipePipe } from './pipes/safe-url-pipe.pipe';

//pages
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { RegisterProjectComponent } from './pages/register-project/register-project.component';
import { ProjectComponent } from './pages/project/project.component';
import { EnterpisesComponent } from './pages/enterpises/enterpises.component';
import { CreateEnterpriseComponent } from './pages/create-enterprise/create-enterprise.component';
import { CreateHistoryComponent } from './pages/create-history/create-history.component';
import { TicketComponent } from './pages/ticket/ticket.component';
import { CreateTicketComponent } from './pages/create-ticket/create-ticket.component';
import { EditTicketComponent } from './pages/edit-ticket/edit-ticket.component';
import { HistoryTicketComponent } from './pages/history-ticket/history-ticket.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    HomeComponent,
    LoginComponent,
    SafeUrlPipePipe,
    RegisterComponent,
    RegisterProjectComponent,
    ProjectComponent,
    EnterpisesComponent,
    CreateEnterpriseComponent,
    CreateHistoryComponent,
    TicketComponent,
    CreateTicketComponent,
    EditTicketComponent,
    HistoryTicketComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireDatabaseModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
