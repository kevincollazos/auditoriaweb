import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Pipe({
  name: 'safeUrlPipe'
})
export class SafeUrlPipePipe implements PipeTransform {

  constructor( private domSanitizer: DomSanitizer ){}

  transform(url): SafeResourceUrl {
    return this.domSanitizer.bypassSecurityTrustResourceUrl(url);
  }

}
