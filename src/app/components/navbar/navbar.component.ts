import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { Location } from '@angular/common';

//services
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {


  constructor(private auth: AuthService, private location: Location) { }

  ngOnInit(): void {}


  salir(){
    this.auth.logout();
    Swal.fire({
      title: 'Cerrar sesion',
      text: "Estas seguro?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Confirmar'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
          'Perfecto!',
          'Has cerrado sesion correctamente',
          'success'
        );
        this.location.replaceState('/login');
        location.reload();
      }
    })
  }

}
